﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoModule.Services
{
    public class FileService
    {
        public static string ReadFile(string path)
        {
            using (StreamReader sr = new StreamReader(path, Encoding.Default))
            {
                string file = string.Empty;
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    file += line;
                }
                return file;
            }
        }

        public static byte[] ReadBytes(string path)
        {
            var bytes = File.ReadAllBytes(path);
            return bytes;
        }

        public static void WriteBytes(string path, byte[] bytes)
        {
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
