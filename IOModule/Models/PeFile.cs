﻿using Common.Models;
using IoModule.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zodiacon.PEParsing;

namespace IoModule.Models
{
    public class PeFile : IFile
    {
        #region fields
        public string FilePath { get; set; }

        public string FileName { get; set; }

        public string Strings { get; set; }

        public byte[] Bytes { get; set; }

        public string Hash { get; set; }

        public PEParser PeStructure { get; set; }
        #endregion

        #region constructor
        public PeFile(string path)
        {
            FilePath = Path.GetFullPath(path);
            FileName = Path.GetFileName(path);
            Bytes = FileService.ReadBytes(path);
            Strings = FileService.ReadFile(path);

            if (Bytes.Count() > 0)
                Hash = CryptoService.GetHash(Bytes);

            
        }
        #endregion

    }
}
