﻿using Analyzer;
using Dal.Context;
using Dal.Models.HeuristicAnalyzer;
using Dal.Models.SignatureAnalyzer;
using IoModule.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ui
{
    class Program
    {
        static void GetTime(Stopwatch stopWatch)
        {
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
        }

        static void GetResult(CommonAnalyzer analyzer, string title)
        {
            var prevColor = Console.ForegroundColor;
            var res = analyzer.GetResult();
            Console.ForegroundColor = res.TextColor;
            Console.WriteLine($"{title}: {res.Comment}");
            Console.ForegroundColor = prevColor;
        }

        static void Analyze(string filepath)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            var file = new PeFile(filepath);
            Console.WriteLine($"Analyze file {file.FileName}");

            var signature = new SignatureAnalyzer(file);
            GetResult(signature, "SignatureAnalyzer");

            var heur = new HeuristicAnalyzer(file);
            GetResult(heur, "HeuristicAnalyzer");

            stopWatch.Stop();
            GetTime(stopWatch);
            Console.WriteLine();
        }

        static void AnalyzeSystem32()
        {
            AnalyzeFolder(@"C:\Windows\System32");
        }

        static void AnalyzeFolder(string folderpath)
        {
            DirectoryInfo d = new DirectoryInfo(folderpath);
            FileInfo[] Files = d.GetFiles("*.*");
            int i = 0;
            foreach (FileInfo file in Files)
            {
                if (file.Length <= 1000000)
                {
                    Console.Write($"{++i}. ");
                    Analyze(file.FullName);
                }
            }
        }

        static void Main(string[] args)
        {
            var path = @"D:\Bitbucket\visual-studio-2017-all-projects\Antivirus";
            AnalyzeFolder(path);
        }
    }
}
