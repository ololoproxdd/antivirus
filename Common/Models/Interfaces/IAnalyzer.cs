﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public interface IAnalyzer
    {
        List<IRule> Rules { get; set; }

        bool IsVirus(double degree);
        IResult GetResult();
    }
}
