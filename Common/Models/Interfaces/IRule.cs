﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public interface IRule
    {
        #region fields
        IFile File { get; set; }
        double DegreeTrust { get; set; }

        List<IRule> Childs { get; set; }
        #endregion

        #region methods
        bool CheckRule();
        double IsVirusDegree();
        string GetRuleText();
        #endregion
    }
}
