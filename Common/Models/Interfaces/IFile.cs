﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zodiacon.PEParsing;

namespace Common.Models
{
    public interface IFile
    {
        string FilePath { get; set; }

        string FileName { get; set; }

        string Strings { get; set; }

        byte[] Bytes { get; set; }

        string Hash { get; set; }

        PEParser PeStructure { get; set; }
    }
}
