﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Models
{
    public interface IResult
    {
        double CurDegree { get; set; }

        string Comment { get; set; }

        ConsoleColor TextColor { get; set; }
    }
}
