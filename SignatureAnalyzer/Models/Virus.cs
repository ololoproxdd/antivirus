﻿using Common.Models;
using Analyzer.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Models
{
    public class Virus : IResult
    {
        #region fields
        public double CurDegree { get; set; }
        public string Comment { get; set; }
        public ConsoleColor TextColor { get; set; }
        #endregion

        #region constructors
        public Virus()
        {
            Comment += "Virus;\n";
            TextColor = ConsoleColor.Red;
        }

        public Virus(double curDegree, string comment) : this()
        {
            CurDegree = curDegree;
            Comment += comment;
        }

        #endregion
    }
}
