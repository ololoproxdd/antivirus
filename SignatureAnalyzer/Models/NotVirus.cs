﻿using Common.Models;
using Analyzer.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Models
{
    public class NotVirus : IResult
    {
        #region fields
        public double CurDegree { get; set; }
        public string Comment { get; set; }
        public ConsoleColor TextColor { get; set; }
        #endregion

        #region constructors
        public NotVirus()
        {
            Comment += "NotVirus;\n";
            TextColor = ConsoleColor.Green;
        }

        public NotVirus(double curDegree, string comment) : this()
        {
            CurDegree = curDegree;
            Comment += comment;
        }
        #endregion
    }
}
