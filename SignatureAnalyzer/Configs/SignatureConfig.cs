﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Configs
{
    public static class SignatureConfig
    {
        #region methods
        public static double HashServiceDegreeTrust()
        {
            var dt = ConfigurationManager.AppSettings["HashServiceDegreeTrust"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 1;
        }
        #endregion
    }
}
