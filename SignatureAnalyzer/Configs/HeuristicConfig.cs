﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Configs
{
    public static class HeuristicConfig
    {
        #region methods
        public static double FileNameServiceDegreeTrust()
        {
            var dt = ConfigurationManager.AppSettings["FileNameServiceDegreeTrust"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 0.1;
        }

        public static double FilePathServiceDegreeTrust()
        {
            var dt = ConfigurationManager.AppSettings["FilePathServiceDegreeTrust"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 0.1;
        }

        public static double PeRegeditServiceDegreeTrust()
        {
            var dt = ConfigurationManager.AppSettings["PeRegeditServiceDegreeTrust"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 1;
        }

        public static double PeWinapiServiceDegreeTrust()
        {
            var dt = ConfigurationManager.AppSettings["PeWinapiServiceDegreeTrust"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 0.5;
        }
        #endregion
    }
}
