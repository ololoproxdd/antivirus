﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Configs
{
    public static class CommonConfig
    {
        #region methods
        public static double NotVirus()
        {
            var dt = ConfigurationManager.AppSettings["NotVirus"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 0.5;
        }

        public static double Suspicious()
        {
            var dt = ConfigurationManager.AppSettings["Suspicious"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 0.8;
        }

        public static double Virus()
        {
            var dt = ConfigurationManager.AppSettings["Virus"];
            if (dt != null && Double.TryParse(dt, out double result))
                return result;

            return 1;
        }

        #endregion
    }
}
