﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Analyzer.Configs;
using Analyzer.Models;
using Common;
using Common.Models;

namespace Analyzer.Analyzers
{
    public class TreeAnalyzer : CommonAnalyzer
    {
        #region fields
        double Degree { get; set; }

        string Comment { get; set; }

        public List<IRule> UpperLvlRules { get; set; }
        #endregion

        #region constructors
        public TreeAnalyzer() : base()
        {
            Degree = 0;
            Comment = "";
        }
        #endregion

        #region methods
        public override IResult GetResult()
        {
            var rules = new List<IRule>(UpperLvlRules);
            foreach (var rule in rules)
            {
                var curDegree = rule.IsVirusDegree();
                if (curDegree > 0)
                {
                    Degree += curDegree;
                    Comment += rule.GetRuleText();

                    //если степень уже говорит о том что это вирус
                    if (IsVirus(Degree))
                        return new Virus(Degree, Comment);

                    //проверяем потомков
                    if (rule.Childs != null && rule.Childs.Count > 0)
                    {
                        UpperLvlRules = rule.Childs;
                        return GetResult();
                    }
                }
            }

            if (Degree < CommonConfig.NotVirus())
                return new NotVirus(Degree, Comment);

            else if (Degree < CommonConfig.Suspicious())
                return new Suspicious(Degree, Comment);

            return new Virus(Degree, Comment);
        }

        #endregion
    }
}
