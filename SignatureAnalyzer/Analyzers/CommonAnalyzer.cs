﻿using Common;
using Common.Models;
using Analyzer.Configs;
using Analyzer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer
{
    public class CommonAnalyzer : IAnalyzer
    {
        #region fields
        public List<IRule> Rules { get; set; }
        #endregion

        #region methods

        public bool IsVirus(double degree)
        {
            if (degree >= CommonConfig.Suspicious())
                return true;

            return false;
        }

        public virtual IResult GetResult()
        {
            double degree = 0;
            string comment = "";

            foreach (var rule in Rules)
            {
                var curDegree = rule.IsVirusDegree();
                if (curDegree > 0)
                {
                    degree += curDegree;
                    comment += rule.GetRuleText();
                }

                //если степень уже говорит о том что это вирус
                if (IsVirus(degree))
                    return new Virus(degree, comment);
            }

            if (degree < CommonConfig.NotVirus())
                return new NotVirus(degree, comment);

            else if (degree < CommonConfig.Suspicious())
                return new Suspicious(degree, comment);

            return new Virus(degree, comment);
        }
        #endregion
    }
}
