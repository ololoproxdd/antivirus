﻿using Analyzer.Configs;
using Analyzer.Services;
using Common;
using Common.Models;
using Analyzer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Analyzer.Rules.HeuristicAnalyzer;
using Analyzer.Analyzers;

namespace Analyzer
{
    public class HeuristicAnalyzer : TreeAnalyzer
    {
        #region fields

        public IFile file;
        #endregion

        #region constructors
        public HeuristicAnalyzer(IFile file) : base()
        {
            this.file = file;
            Rules = InitRules();
            UpperLvlRules = Rules;
        }
        #endregion

        #region methods
        List<IRule> InitRules()
        {
            var rules = new List<IRule>();

            #region winlock rule 1
            var fileNameDt = HeuristicConfig.FileNameServiceDegreeTrust();
            var fileName = new FileNameRule(fileNameDt, file);
            //rules.Add(hashService);

            var filePathDT = HeuristicConfig.FilePathServiceDegreeTrust();
            var filePath = new FilePathRule(filePathDT, file);
            //rules.Add(peService);

            var peWinapiDt = HeuristicConfig.PeWinapiServiceDegreeTrust();
            var peWinapi = new PeWinapiRule(peWinapiDt, file);
            //rules.Add(peWinapi);

            var peRegeditDt = HeuristicConfig.PeRegeditServiceDegreeTrust();
            var peRegedit = new PeRegeditRule(peRegeditDt, file);
            //rules.Add(peRegedit);

            var PeDt = 0.001;
            var peCheck = new PeCheckRule(PeDt, file);
            peCheck.Childs.Add(fileName);
            peCheck.Childs.Add(filePath);
            peCheck.Childs.Add(peWinapi);
            peCheck.Childs.Add(peRegedit);
            //rules.Add(peCheck);

            var MzDt = 0.001;
            var MzCheck = new MzCheckRule(MzDt, file);
            MzCheck.Childs.Add(peCheck);
            rules.Add(MzCheck);
            #endregion

            return rules;
        }

        #endregion
    }
}
