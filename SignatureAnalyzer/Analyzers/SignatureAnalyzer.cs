﻿using Common;
using Common.Models;
using Analyzer.Configs;
using Analyzer.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Analyzer.Rules.SignatureAnalyzer;

namespace Analyzer
{
    public class SignatureAnalyzer : CommonAnalyzer
    {
        #region fields
        public IFile file;
        #endregion

        #region constructors
        public SignatureAnalyzer(IFile file)
        {
            this.file = file;
            Rules = InitRules();
        }
        #endregion

        #region methods
        List<IRule> InitRules()
        {
            var rules = new List<IRule>();

            var windowsBlockedRule = new WindowsBlockedRule(1, file);
            rules.Add(windowsBlockedRule);

            var hashDegreeTrust = SignatureConfig.HashServiceDegreeTrust();
            var hashRule = new HashRule(hashDegreeTrust, file);
            rules.Add(hashRule);

            return rules;
        }

        #endregion
    }
}
