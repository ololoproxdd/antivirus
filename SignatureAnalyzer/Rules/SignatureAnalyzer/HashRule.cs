﻿using Analyzer.Rules;
using Common;
using Common.Models;
using Dal.Context;
using IoModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Services
{
    public class HashRule : CommonRule
    {

        #region constructors
        public HashRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        public override bool CheckRule()
        {
            var hash = File.Hash;
            if (hash == null)
                return false;

            using (MssqlContext db = new MssqlContext())
            {
                var hashes = db.FileHashes;
                var count = hashes.Where(w => w.Value == hash).Count();
                return count > 0;
            }
        }
        public override string GetRuleText()
        {
            return $"File hash {File.Hash} is in the database of malicious files;\n";
        }
        #endregion
    }
}
