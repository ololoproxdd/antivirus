﻿using Common;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Rules.SignatureAnalyzer
{
    public class WindowsBlockedRule : CommonRule
    {
        #region constructors
        public WindowsBlockedRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        public override bool CheckRule()
        {
            //Windows заблокирован!
            var bytes = new byte[] { 0x57, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x73, 0x20, 0xe7, 0xe0, 0xe1, 0xeb, 0xee, 0xea, 0xe8, 0xf0, 0xee, 0xe2, 0xe0, 0xed, 0x21};

            int offset = 0x542c0 + 0xA;
            var bytesInFile = File.Bytes.Skip(offset).Take(bytes.Count()).ToArray();

            if (bytesInFile.Count() == bytes.Count())
            {
                for (int i = 0; i < bytes.Count(); i++)
                    if (bytes[i] != bytesInFile[i])
                        return false;

                return true;
            }

            return false;
        }

        public override string GetRuleText()
        {
            return $"File contains string 'Windows заблокирован!';\n";
        }
        #endregion
    }
}
