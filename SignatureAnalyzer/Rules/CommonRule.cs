﻿using Common;
using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Rules
{
    public class CommonRule : IRule
    {
        #region fields
        public IFile File { get; set; }

        public double DegreeTrust { get; set; }

        public List<IRule> Childs { get; set; }

        public List<string> ContainsDangerous { get; set; }
        #endregion

        #region constructor
        public CommonRule(IFile file, double degreeTrust)
        {
            File = file;
            DegreeTrust = degreeTrust;
            Childs = new List<IRule>();
        }
        #endregion

        #region methods
        public virtual bool CheckRule()
        {
            return false;
        }

        public virtual string GetRuleText()
        {
            return "Common error;\n";
        }

        public double IsVirusDegree()
        {
            var check = CheckRule();
            return check ? DegreeTrust : 0;
        }
        #endregion
    }
}
