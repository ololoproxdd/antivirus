﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Rules.HeuristicAnalyzer
{
    public class MzCheckRule : CommonRule
    {
        #region constructors
        public MzCheckRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        public override bool CheckRule()
        {
            uint offset = 0;
            if (File.Bytes[offset] == 77
                && File.Bytes[offset + 1] == 90)
                return true;

            return false;
        }

        public override string GetRuleText()
        {
            return $"It's MZ file;\n";
        }
        #endregion
    }
}
