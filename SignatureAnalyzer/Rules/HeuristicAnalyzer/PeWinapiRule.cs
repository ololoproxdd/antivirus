﻿using Analyzer.Rules;
using Common;
using Common.Models;
using Dal.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Services
{
    public class PeWinapiRule : CommonRule
    {
        #region fields
        List<string> ContainsDangerous;
        #endregion

        #region constructors
        public PeWinapiRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }

        #endregion

        #region methods

        public override bool CheckRule()
        { 
            var strings = File.Strings.ToLower();
            if (strings == null)
                return false;

            using (MssqlContext db = new MssqlContext())
            {
                var winApiWarnings = db.WinApiWarnings;
                ContainsDangerous = winApiWarnings?.Where(w => strings.Contains(w.Value.ToLower()))
                    ?.Select(w => w.Value)?.ToList() ?? null;
                return ContainsDangerous != null ? ContainsDangerous.Count > 0 : false;
            }
        }

        public override string GetRuleText()
        {
            var result = $"File contains dangerous winapi import methods:";
            if (ContainsDangerous != null)
            {
                foreach (var item in ContainsDangerous)
                    result += $"\n{item}";
            }
            result += ";\n";
            return result;
        }
        #endregion
    }
}
