﻿using Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Rules.HeuristicAnalyzer
{
    class PeCheckRule : CommonRule
    {
        #region constructors
        public PeCheckRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        private uint GetPeOffset(byte[] exe)
        {
            uint offset = 60;
            uint pe_offset =
                (uint)(exe[offset] +
                exe[offset + 1] * (int)Math.Pow(16, 2) +
                exe[offset + 2] * (int)Math.Pow(16, 4) +
                exe[offset + 3] * (int)Math.Pow(16, 6));
            return pe_offset;
        }

        public override bool CheckRule()
        {
            uint offset = GetPeOffset(File.Bytes);
            if (File.Bytes[offset] == 80
                && File.Bytes[offset + 1] == 69
                && File.Bytes[offset + 2] == 0
                && File.Bytes[offset + 3] == 0)
                return true;

            return false;
        }

        public override string GetRuleText()
        {
            return $"It's PE file;\n";
        }
        #endregion
    }
}
