﻿using Analyzer.Rules;
using Common;
using Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Services
{
    public class FilePathRule : CommonRule
    {
        #region constructors
        public FilePathRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        public override bool CheckRule()
        {
            var tempPath = Path.GetTempPath();
            var windowsPath = Environment.GetEnvironmentVariable("SystemRoot");

            List<string> warningPaths = new List<string>{ tempPath, windowsPath };

            return warningPaths.Any(w => w.ToLower() == File.FilePath.ToLower());
        }

        public override string GetRuleText()
        {
            return $"File path {File.FilePath} is suspicious;\n";
        }
        #endregion
    }
}
