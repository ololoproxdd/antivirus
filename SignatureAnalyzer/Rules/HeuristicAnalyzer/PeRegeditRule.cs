﻿using Analyzer.Rules;
using Common;
using Common.Models;
using Dal.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Services
{
    public class PeRegeditRule : CommonRule
    {
        #region constructors
        public PeRegeditRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        public override bool CheckRule()
        {
            var strings = File.Strings.ToLower();
            if (strings == null)
                return false;

            using (MssqlContext db = new MssqlContext())
            {
                var regeditWarnings = db.RegeditWarnings;
                ContainsDangerous = regeditWarnings?.Where(w => strings.Contains(w.Value.ToLower()))
                    ?.Select(w => w.Value)?.ToList() ?? null;
                return ContainsDangerous != null ? ContainsDangerous.Count > 0 : false;
            }
        }

        public override string GetRuleText()
        {
            var result = $"File contains dangerous registry change:";
            if (ContainsDangerous != null)
            {
                foreach (var item in ContainsDangerous)
                    result += $"\n{item}";
            }
            result += ";\n";
            return result;
        }
        #endregion
    }
}
