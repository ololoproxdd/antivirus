﻿using Analyzer.Rules;
using Common;
using Common.Models;
using Dal.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Analyzer.Services
{
    public class FileNameRule : CommonRule
    {
        #region constructors
        public FileNameRule(double degreeTrust, IFile file) : base(file, degreeTrust)
        {
        }
        #endregion

        #region methods
        public override bool CheckRule()
        {
            if (File.FileName == null)
                return false;

            using (MssqlContext db = new MssqlContext())
            {
                var names = db.FileNames;
                var count = names.Where(w => w.Value.ToLower() == File.FileName.ToLower()).Count();
                return count > 0 ? true : false;
            }
        }

        public override string GetRuleText()
        {
            return $"File name {File.FileName} is suspicious;\n";
        }
        #endregion
    }
}
