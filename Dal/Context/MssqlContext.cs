﻿using Dal.Models.HeuristicAnalyzer;
using Dal.Models.SignatureAnalyzer;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal.Context
{
    public class MssqlContext : DbContext
    {
        public MssqlContext() : base("MssqlContext")
        {
        }

        #region signature
        public DbSet<FileHash> FileHashes { get; set; }

        #endregion

        #region heuristic
        public DbSet<FileName> FileNames { get; set; }

        public DbSet<RegeditWarning> RegeditWarnings { get; set; }

        public DbSet<WinApiWarning> WinApiWarnings { get; set; }
        #endregion
    }
}
